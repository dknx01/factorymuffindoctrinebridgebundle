#FactoryMuffinDoctrineBridgeBundle

A bridge bundle for using factory muffin (https://github.com/thephpleague/factory-muffin) with doctrine store inside a symfony project

##Services
_factory_muffin_doctrine_bridge_: the factory muffin instace with doctrine entity manger as store
_factory_muffin_doctrine_repository_store_: the instance of a factory muffin store witth doctrine as store

##test classes:
_Dknx01\FactoryMuffinDoctrineBridge\TestCase\FactoryMuffinTestCase_: An extended Webtestcase with an insatnciated bridge under _`static::$muffin`_