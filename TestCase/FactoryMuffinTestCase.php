<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 12.03.17 16:23
 */

namespace Dknx01\FactoryMuffinDoctrineBridge\TestCase;

use League\FactoryMuffin\FactoryMuffin;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FactoryMuffinTestCase extends WebTestCase
{
    /**
     * @var FactoryMuffin
     */
    protected static $muffin;

    /**
     * @inheritdoc
     */
    protected static function createClient(array $options = array(), array $server = array())
    {
        $client = parent::createClient($options, $server);

        static::$muffin = $client->getContainer()->get('factory_muffin_doctrine_bridge');

        return $client;
    }
}
